Ext.application({
    name: 'ISTH',
    appFolder: '/assets/ui/',
    stores: [
        'ISTH.store.Products',
        'ISTH.store.Variants'
    ],
    models: [
        'ISTH.model.Product',
        'ISTH.model.ProductDetail',
        'ISTH.model.Variant'
    ],
    views: ['ui.view.*'],
    launch: function() {
        var me = this;

        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            alias: 'widget.productviewer',
            defaults: {
                layout: 'anchor'
            },
            items: [
                {
                    region: 'west',
                    title: 'Product List',
                    collapsible: true,
                    layout: 'fit',
                    width: 250,
                    items: [
                        Ext.create('ISTH.view.productlist.ProductList')
                    ]
                },
                {
                    region: 'center',
                    title: 'Product Detail View',
                    items: [
                        Ext.create('ISTH.view.productdetails.ProductDetails')
                    ]
                }
            ]
        });
    }
});