Ext.define('ISTH.store.Variants', {
    extend: 'Ext.data.Store',
    model: 'ISTH.model.Variant',
    storeId: 'variants',
    statefulFilters: true,
    data: []
});