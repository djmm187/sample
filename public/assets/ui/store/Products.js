Ext.define('ISTH.store.Products', {
    extend: 'Ext.data.Store',
    model: 'ISTH.model.Product',
    storeId: 'products',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'shopify/get?path=/admin/products.json?fields=id,title',
        queryMode: 'remote',
        reader: {
            type: 'json',
            rootProperty: 'products'
        }
    },

    sorters : {
        property : 'created_at',
        direction : 'DESC'
    }
});