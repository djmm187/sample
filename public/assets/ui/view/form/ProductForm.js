Ext.define('ISTH.view.form.ProductForm', {
    extend: 'Ext.window.Window',
    title: 'test',
    height: 400,
    width: 400,
    layout: 'fit',
    modal: true,
    bodyPadding: 10,
    closeAction: 'destroy',
    initComponent: function(){
        var me = this;

        me.items = [
            {
                xtype: 'form',
                border: 0,
                defaults: {
                    allowBlank: false,
                    xtype: 'textfield',
                    flex: 1
                },
                items: [
                    {
                        name: 'title',
                        fieldLabel: 'Title',
                    },
                    {
                        name: 'body_html',
                        fieldLabel: 'Description'
                    },
                    {

                        fieldLabel: 'Product Type',
                        name: 'product_type'
                    },
                    {

                        fieldLabel: 'Tags',
                        name: 'tags'
                    },
                    {

                        fieldLabel: 'Vendor',
                        name: 'vendor'
                    }
                ]
            }
        ];

        me.buttons = [
            {
                text: 'Cancel',
                handler: function(btn) {
                    btn.up('window').close();
                }
            }, {
                text: 'Save',
                formBind: true,
                handler: function(btn) {
                    var win = btn.up('window'),
                        form = win.down('form').getForm(),
                        vals, data;

                    if (form.isValid()) {
                        vals = form.getValues();
                        Ext.Ajax.request({
                            url: me.url || 'shopify/post?path=/admin/products.json',
                            method: me.method || 'POST',
                            jsonData: {
                                product: vals
                            },
                            success: function(response) {
                                me.successCallback(
                                    new ISTH.model.Product(
                                        Ext.decode(response.responseText).product
                                    ),
                                    vals
                                );

                                me.close();
                            },
                            failure: function(response) {
                                console.error(response.responseText);
                            }
                        })
                    }
                }
            }
        ];

        me.callParent();
    }
});