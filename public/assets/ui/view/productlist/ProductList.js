Ext.define('ISTH.view.productlist.ProductList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.productlist',
    itemId: 'productList',
    border: 0,
    viewConfig: {
        sortable: false,
        markDirty: false
    },
    viewModel: {
        data: {
            title: ''
        }
    },
    layout: 'fit',
    hideHeaders: true,
    initComponent : function () {
        var me = this,
            vm = me.getViewModel();

        me.columns = [
            {
                dataIndex: 'title',
                flex: 1,
                menuDisabled: true,
                sortable: false,
                listeners: {
                    click: me.productClick
                }
            },
            {
                xtype : 'actioncolumn',
                width: 30,
                align: 'center',
                items : [
                    {
                        iconCls: 'fa fa-lg fa-trash-o glyph',
                        tooltip: 'Delete',
                        handler : me.productDelete
                    }
                ]
            }
        ];

        me.bbar = ['->', {
            xtype: 'button',
            text: 'Add Product',
            itemId: 'newProduct',
            handler: me.addProduct
        }];

        me.tbar = [
            {
                xtype: 'textfield',
                emptyText: 'Title Search',
                bind: {
                    value: '{title}'
                }
            }
        ];

        me.store = Ext.create('ISTH.store.Products');

        // bind onchange to the title property in our viewModel
        vm.bind('{title}', function(value) {
            clearTimeout(me.searchWaiter);

            if (!value) {
                me.store.clearFilter();
                return;
            }

            me.searchWaiter = setTimeout(function() {
                // @TODO: uncomment me when shopify makes this public
                // me.store.proxy.url = 'shopify/get?path=/admin/variants/search.json?query=title:' + value;
                // me.store.load();

                // for now do a local sort
                me.store.filter('title', value);
            }, 300);
        });

        me.callParent();
    },

    productClick: function(grid, cell, cellidx, cellcol, event, record) {
        Ext.Ajax.request({
            url: 'shopify/get?path=/admin/products/'+record.get('id') +'.json',
            method: 'GET',
            success: function(response) {
                var product = new ISTH.model.Product(Ext.decode(response.responseText).product),
                    panel = grid.up('viewport').down('[itemId=productDetails]'),
                    picture = panel.down('[itemId=picture]'),
                    details = panel.down('[itemId=details]'),
                    imgMeta = product.get('image'),
                    imgSr = (imgMeta && (imgMeta['src'] !== undefined)) ?
                                imgMeta['src'] : '/assets/images/NoImageAvailable.jpg';

                    picture.setHtml(
                        '<div class="image_container">' +
                        '<img class="productimage" src="' + imgSr +'" width=200, height=200 />' +
                        '</div>'
                    );

                    details.loadRecord(product);
                    Ext.getStore('variants').removeAll();
                    Ext.getStore('variants').setData(product.get('variants'));
                    panel.down('[itemId=variantData]').hide();
                    details.down('combobox').setValue(null);
                    panel.show();
            },
            failure: function(response) {
                console.error(response.responseText);
            }
        })
    },

    addProduct: function(btn) {
        Ext.create('ISTH.view.form.ProductForm', {
            title: 'Add a new Product',
            successCallback: function(record) {
                Ext.getStore('products').add(record);
            }
        }).show();
    },

    productDelete: function(view, rowIndex, colIndex, item, e, record, row) {
        var record = view.getStore().getAt(rowIndex);
        Ext.Msg.show({
            title:'DELETING ' + record.get('title'),
            message: 'Are you sue you want to delete this product??',
            buttons: Ext.Msg.YESNO,
            closeAction: 'destroy',
            fn: function(btn) {
                if (btn === 'yes') {
                    view.up('grid').confirmDelete.call(view, record);
                }
            }
        });
    },

    confirmDelete: function(record) {
        var recordId = record.get('id');

        Ext.Ajax.request({
            url: 'shopify/delete?path=/admin/products/' + record.get('id') +'.json',
            method: 'DELETE',
            jsonData: {},
            success: function(response) {
                var store = Ext.getStore('products'),
                    recordIdx = Ext.getStore('products').findExact('id', recordId);

                store.removeAt(recordIdx);
            },
            failure: function(response) {
                console.error(response.responseText);
            }
        })
    }

});