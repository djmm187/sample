Ext.define('ISTH.view.productdetails.ProductDetails', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.productdetails',
    layout: 'fit',
    itemId: 'productDetails',
    border: 0,
    viewConfig: {
        sortable: false,
        markDirty: false
    },
    initComponent : function () {
        var me = this,
            variantStore = Ext.create('ISTH.store.Variants'),
            defaults = {
                readOnly: true,
                readOnlyCls: 'read-only',
                labelAlign: 'left',
                xtype: 'displayfield'
            };

        me.hidden = true;
        me.items = [
            {
                xtype: 'panel',
                title: null,
                layout: 'hbox',
                border: 0,
                defaults: {
                    border: 0,
                    layout: 'anchor'
                },
                items: [{
                    xtype: 'panel',
                    flex: 1,
                    itemId: 'picture',
                    bodyPadding: 10,
                    margin: '40 10',
                    html: ''
                },
                {
                    xtype: 'form',
                    flex: 3,
                    itemId: 'details',
                    defaults: defaults,
                    items: [
                        {

                            name: 'title',
                            fieldLabel: 'Title'
                        },
                        {

                            name: 'body_html',
                            fieldLabel: 'Description'
                        },
                        {

                            fieldLabel: 'Product Type',
                            name: 'product_type'
                        },
                        {

                            fieldLabel: 'Tags',
                            name: 'tags'
                        },
                        {
                            xtype: 'combobox',
                            store: variantStore,
                            fieldLabel: 'Choose Variant',
                            queryMode: 'local',
                            displayField: 'option1',
                            readOnly: false,
                            typeAhead: false,
                            valueField: 'id',
                            emptyText: 'Select A Variant',
                            listeners: {
                                change: function(me, newValue, oldValue, eOpts) {
                                    var store = me.getStore(),
                                        recordId = store.findExact('id', newValue),
                                        record = store.getAt(recordId),
                                        variantData = me.up('form').down('[itemId=variantData]'),
                                        metaFields = variantData.query('field');

                                    if (!!newValue) {
                                        Ext.each(metaFields, function(field){
                                            field.setValue(record.get(field.getName()))
                                        });

                                        return variantData.show();
                                    }

                                    variantData.hide();
                                }
                            }
                        },
                        {
                            xtype: 'fieldcontainer',
                            itemId: 'variantData',
                            border: 0,
                            hidden: true,
                            defaults: defaults,
                            items: [
                                {
                                    xtype: 'displayfield',
                                    name: 'sku',
                                    fieldLabel: 'SKU'
                                },
                                {
                                    xtype: 'displayfield',
                                    name: 'product_id',
                                    fieldLabel: 'Product ID'
                                },
                                {
                                    xtype: 'displayfield',
                                    name: 'price',
                                    fieldLabel: 'Price'
                                },
                                {
                                    xtype: 'displayfield',
                                    name: 'inventory_quantity',
                                    fieldLabel: 'Available Quantity'
                                },
                            ]
                        }
                    ],
                    tbar: [
                        '->',
                        {
                            xtype: 'button',
                            text: 'Edit Product',
                            itemId: 'newProduct',
                            handler: me.editProduct
                        }
                    ]
                }]
            }
        ];
        me.callParent();
    },

    editProduct: function(btn) {
        var form = btn.up('panel').getForm(),
            formRecord = form.getRecord(),
            win;

        win = Ext.create('ISTH.view.form.ProductForm', {
            title: 'Editing: ' + formRecord.get('title'),
            successCallback: function(record, formValues) {
                var store = Ext.getStore('products'),
                    recordId = store.findExact('id', formRecord.get('id')),
                    rec = store.getAt(recordId);

                Ext.Object.each(formValues, function(k, v) {
                    rec.set(k, v);
                });
                rec.commit();
                form.loadRecord(rec);
            },
            method: 'PUT',
            url: 'shopify/put?path=/admin/products/'+ formRecord.get('id') + '.json'
        });

        win.down('form').getForm().loadRecord(formRecord);
        win.show();
    }
});
