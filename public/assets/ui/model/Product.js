Ext.define('ISTH.model.Product', {
    extend : 'Ext.data.Model',
    fields : [
        'id',
        'title',
        'created_at',
        'tags',
        'handle',
        'product_type'
    ]
});