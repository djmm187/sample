Ext.define('ISTH.model.Variant', {
    extend : 'Ext.data.Model',
	fields : [
		'id',
		'sku',
		'price',
		'option1',
		'product_id'
	],
	data: []
});